import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Row, Col, Form, ButtonToolbar, Button } from 'react-bootstrap';
import { userActions } from '../_actions';

class LoginPage extends React.Component {
    constructor(props) {
        super(props);

        // reset login status
        this.props.dispatch(userActions.logout());

        this.state = {
            username: '',
            password: '',
            submitted: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }

    handleSubmit(e) {
        e.preventDefault();

        this.setState({ submitted: true });
        const { username, password } = this.state;
        const { dispatch } = this.props;
        if (username && password) {
            dispatch(userActions.login(username, password));
        }
    }

    render() {
        const { loggingIn } = this.props;
        const { username, password, submitted } = this.state;
        return (
            <Row>
                <Col md={{ span: 6, offset: 3 }}>
                    <h2>Вход</h2>
                    <Form name="form" onSubmit={this.handleSubmit}>
                        <Form.Group>
                            <Form.Label>Email</Form.Label>
                            <Form.Control name="username" value={username} onChange={this.handleChange} />
                            {submitted && !username &&
                            <Form.Text className='text-danger'>Username is required</Form.Text>
                            }
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Пароль</Form.Label>
                            <Form.Control name="password" type="password" value={password} onChange={this.handleChange} />
                            {submitted && !password &&
                            <Form.Text className='text-danger'>Password is required</Form.Text>
                            }
                        </Form.Group>
                        <ButtonToolbar>
                            <Button variant="primary" type="submit">Вход</Button>
                            {loggingIn &&
                            <div className="spinner-border" role="status">
                                <span className="sr-only">Loading...</span>
                            </div>
                            }
                            <Link to="/register" className="btn btn-link">Регистрация</Link>
                        </ButtonToolbar>
                    </Form>
                </Col>
            </Row>
        );
    }
}

function mapStateToProps(state) {
    const { loggingIn } = state.authentication;
    return {
        loggingIn
    };
}

const connectedLoginPage = connect(mapStateToProps)(LoginPage);
export { connectedLoginPage as LoginPage }; 