import React from 'react';
import {connect} from "react-redux";

class UserPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {

        }
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-sm-2 list-group list-group-flush">
                        <a className="list-group-item list-group-item-action">Моя страница</a>
                        <a className="list-group-item list-group-item-action">Подписки</a>
                        <a className="list-group-item list-group-item-action">Подписчики</a>
                        <a className="list-group-item list-group-item-action">Мои записи</a>
                    </div>

                    <div className="col-sm-10">
                        <div className="content">
                            <h2>Иван Иванов</h2>
                            <div className="photo">

                            </div>

                            <div className="userData">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {

    };
}

const connectedUserPage = connect(mapStateToProps)(UserPage);
export { connectedUserPage as UserPage };