import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Row, Col, Form, ButtonToolbar, Button } from 'react-bootstrap';

import { userActions } from '../_actions';

class RegisterPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            user: {
                email: '',
                type: 10,
                name: '',
                password: '',
                account_image: ''
            },
            submitted: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        const { name, value } = event.target;
        const { user } = this.state;
        this.setState({
            user: {
                ...user,
                [name]: value
            }
        });
    }

    handleSubmit(event) {
        event.preventDefault();

        this.setState({ submitted: true });
        const { user } = this.state;
        const { dispatch } = this.props;
        if (user.email && user.type && user.name && user.password && user.account_image) {
            dispatch(userActions.register(user));
        }

        console.log(this.state.submitted);
    }

    handleImageChange(e) {
        e.preventDefault();
        const { user } = this.state;

        let reader = new FileReader();
        let file = e.target.files[0];

        reader.onloadend = () => {
            this.setState({
                file: file,
                imagePreviewUrl: reader.result,
                user: {
                    ...user,
                    account_image: reader.result
                }
            });
        };
        reader.readAsDataURL(file);
    }

    render() {
        const { registering  } = this.props;
        const { user, submitted } = this.state;

        let {imagePreviewUrl} = this.state;
        let $imagePreview = null;
        if (imagePreviewUrl) {
            $imagePreview = (<img src={imagePreviewUrl} />);
        } else {
            $imagePreview = (<img src="https://cdn.iconscout.com/icon/free/png-256/avatar-372-456324.png" />);
        }

        return (
            <Row>
                <Col md={{ span: 6, offset: 3 }}>
                    <h2>Регистрация</h2>
                    <Form name="form" onSubmit={this.handleSubmit}>
                        <Form.Group>
                            <div className="previewComponent">
                                <input className="fileInput" type="file" onChange={(e)=>this.handleImageChange(e)} />
                                <div className="imgPreview">
                                    {$imagePreview}
                                </div>
                            </div>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Email</Form.Label>
                            <Form.Control name="email" value={user.email} onChange={this.handleChange} />
                            {submitted && !user.email &&
                            <Form.Text className='text-danger'>Email is required</Form.Text>
                            }
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Тип пользователя</Form.Label>
                            <Form.Control name="type" value={user.type} onChange={this.handleChange} type="number" />
                            {submitted && !user.type &&
                            <Form.Text className='text-danger'>User type is required</Form.Text>
                            }
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Имя</Form.Label>
                            <Form.Control name="name" value={user.name} onChange={this.handleChange} />
                            {submitted && !user.name &&
                            <Form.Text className='text-danger'>Username is required</Form.Text>
                            }
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Пароль</Form.Label>
                            <Form.Control name="password" type="password" value={user.password} onChange={this.handleChange} />
                            {submitted && !user.password &&
                            <Form.Text className='text-danger'>Password is required</Form.Text>
                            }
                        </Form.Group>
                        <ButtonToolbar>
                            <Button variant="primary" type="submit">Зарегистрироваться</Button>
                            {registering &&
                            <div className="spinner-border" role="status">
                                <span className="sr-only">Loading...</span>
                            </div>
                            }
                            <Link to="/login" className="btn btn-link">Отмена</Link>
                        </ButtonToolbar>
                    </Form>
                </Col>
            </Row>
        );
    }
}

function mapStateToProps(state) {
    const { registering } = state.registration;
    return {
        registering
    };
}

const connectedRegisterPage = connect(mapStateToProps)(RegisterPage);
export { connectedRegisterPage as RegisterPage };